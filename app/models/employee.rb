class Employee < ActiveRecord::Base

  validates_presence_of :name, :rate
  validates_numericality_of :rate, :greater_than_or_equal_to => 7.25
  self.auto_html5_validation = false
end
